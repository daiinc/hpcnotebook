#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "redis_store.h"

/* global variables */
redisContext *c;

int connect_db(const char *host, int port) {
	struct timeval timeout = { 1, 500000 }; // 1.5 seconds
	c = redisConnectWithTimeout(host, port, timeout);
	if (c == NULL || c->err) {
		if (c) {
			printf("Connection error: %s\n", c->errstr);
			redisFree(c);
		} else {
			printf("Connection error: can't allocate redis context\n");
		}
		return -1;
	}
	return 0;
}

void close_db() {
	/* Disconnects and frees the context */
	if (c != NULL)
		redisFree(c);
}

int insert(char *src, u_int32_t ssize, char *dst, u_int32_t dsize,
		u_int32_t type, u_int64_t ts, char *val, u_int32_t vsize) {

	u_int32_t vertex_size = ssize * sizeof(char) + sizeof(u_int16_t);
	char *key = (char *) malloc(vertex_size);
	memcpy(key, &ssize, sizeof(u_int16_t));
	memcpy(key + sizeof(u_int16_t), src, ssize);

	u_int32_t total = (dsize + vsize) * sizeof(char) + 2 * sizeof(u_int32_t)
			+ sizeof(u_int64_t) + sizeof(u_int16_t);

	char *item = (char *) malloc(total);
	memcpy(item, &dsize, sizeof(u_int16_t));
	memcpy(item + sizeof(u_int16_t), dst, dsize);
	memcpy(item + sizeof(u_int16_t) + dsize, &ts, sizeof(u_int64_t));
	memcpy(item + sizeof(u_int16_t) + dsize + sizeof(u_int64_t), &type,
			sizeof(u_int32_t));
	memcpy(
			item + sizeof(u_int16_t) + dsize + sizeof(u_int64_t)
					+ sizeof(u_int32_t), &vsize, sizeof(u_int32_t));
	memcpy(
			item + sizeof(u_int16_t) + dsize + sizeof(u_int64_t)
					+ 2 * sizeof(u_int32_t), val, vsize);

	redisReply *reply;
	/* add an item into sorted list */
	reply = redisCommand(c, "ZADD %b CH 0 %b", key, (size_t) vertex_size, item,
			(size_t) total);

	//printf("ZADD: %lld, Context: %d\n", reply->integer, c->err);
	freeReplyObject(reply);
	free(key);
	free(item);
	return c->err;
}

char* get(char *src, u_int32_t ssize, char *dst, u_int32_t dsize,
		u_int32_t type, u_int64_t ts, u_int32_t *vsize) {

	u_int32_t vertex_size = ssize * sizeof(char) + sizeof(u_int16_t);
	char *key = (char *) malloc(vertex_size);
	memcpy(key, &ssize, sizeof(u_int16_t));
	memcpy(key + sizeof(u_int16_t), src, ssize);

	u_int32_t total = dsize * sizeof(char) + sizeof(u_int32_t)
			+ sizeof(u_int64_t) + sizeof(u_int16_t);

	redisReply *reply;
	/* get an item from sorted list */
	reply = redisCommand(c, "ZRANGE %b 0 -1", key, (size_t) vertex_size);
	char *rtn = NULL;

	for (int i = 0; i < reply->elements; i++) {
		int next = 0;
		char *p = reply->element[i]->str;

		int t_dsize = (u_int16_t)(*p);
		char *t_dst = p + sizeof(u_int16_t);
		u_int64_t t_ts = (u_int64_t)(*(p + sizeof(u_int16_t) + t_dsize));
		u_int32_t t_type = (u_int32_t)(
				*(p + sizeof(u_int16_t) + t_dsize + sizeof(u_int64_t)));

		//printf("get value: %s\n", (p + sizeof(u_int16_t) + t_dsize + sizeof(u_int64_t) + sizeof(u_int32_t) * 2));

		if (t_dsize == dsize && memcmp(t_dst, dst, t_dsize) == 0 && t_ts == ts
				&& t_type == type) {

			rtn = (p + sizeof(u_int16_t) + t_dsize + sizeof(u_int64_t)
					+ sizeof(u_int32_t));
			(*vsize) = (u_int32_t) * rtn;
			rtn += sizeof(u_int32_t);
			return rtn;
		}
	}
	return NULL;
}

int del(char *src, u_int32_t ssize, char *dst, u_int32_t dsize, u_int32_t type,
		u_int64_t ts) {
	return 0;
}

void iterate_print() {
	redisReply *reply;
	int cursor = 0;
	/* iterate all keys */

	while (1) {
		char command[32] = { 0 };
		sprintf(command, "SCAN %d", cursor);

		reply = redisCommand(c, command);

		if (reply->elements != 2) {
			printf("Internal Errors in SCAN: Return More than 2 elements\n");
			return;
		}

		//printf("cursor: %s\n", reply->element[0]->str);
		cursor = atoi(reply->element[0]->str);
		if (cursor == 0) {
			break;
		}

		redisReply *tmp = reply->element[1];

		for (int j = 0; j < tmp->elements; j++) {
			size_t length = tmp->element[j]->len;
			char *key = tmp->element[j]->str;
			u_int16_t key_len = (u_int16_t)(*key);

			reply = redisCommand(c, "ZRANGE %b 0 -1", key, length);
			char *rtn = NULL;

			for (int i = 0; i < reply->elements; i++) {
				int next = 0;
				char *p = reply->element[i]->str;

				int t_dsize = (u_int16_t)(*p);
				char *t_dst = p + sizeof(u_int16_t);
				u_int64_t t_ts = (u_int64_t)(
						*(p + sizeof(u_int16_t) + t_dsize));
				u_int32_t t_type = (u_int32_t)(
						*(p + sizeof(u_int16_t) + t_dsize + sizeof(u_int64_t)));

				printf("Key: ");
				for (int k = 0; k < key_len; k++) {
					printf("%02x ",
							(u_int8_t)(
									*(key + sizeof(u_int16_t) + sizeof(char) * k)));
				}
				printf("--- Value: ");
				printf("ts: %" PRIu64 ", type: %02d, Destination Key: ", t_ts, t_type);
				for (int k = 0; k < t_dsize; k++) {
					printf("%02x ", (u_int8_t)(*(t_dst + sizeof(char) * k)));
				}
				printf("\n");
			}
		}
	}
}

void iterate_json() {
}

