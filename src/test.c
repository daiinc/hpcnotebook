#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "redis_store.h"

int main(int argc, char **argv) {

	/*
	redisContext *c;

	char db_host[32] = "localhost";
	int db_port = 6379;

	struct timeval timeout = { 1, 500000 }; // 1.5 seconds
	c = redisConnectWithTimeout(db_host, db_port, timeout);
	if (c == NULL || c->err) {
		if (c) {
			printf("Connection error: %s\n", c->errstr);
			redisFree(c);
		} else {
			printf("Connection error: can't allocate redis context\n");
		}
		return -1;
	}

	redisReply *reply;
	int cursor = 0;

	while (1) {
		char command[32] = { 0 };
		sprintf(command, "SCAN %d", cursor);

		reply = redisCommand(c, command);

		if (reply->elements != 2) {
			printf("Internal Errors in SCAN: Return More than 2 elements\n");
			return 0;
		}

		//printf("cursor: %s\n", reply->element[0]->str);
		cursor = atoi(reply->element[0]->str);
		if (cursor == 0) {
			break;
		}

		redisReply *tmp = reply->element[1];

		for (int j = 0; j < tmp->elements; j++) {
			size_t length = tmp->element[j]->len;
			char *key = tmp->element[j]->str;
			u_int16_t key_len = (u_int16_t)(*key);

			reply = redisCommand(c, "ZRANGE %b 0 -1", key, length);
			char *rtn = NULL;

			for (int i = 0; i < reply->elements; i++) {
				int next = 0;
				char *p = reply->element[i]->str;

				int t_dsize = (u_int16_t)(*p);
				char *t_dst = p + sizeof(u_int16_t);
				u_int64_t t_ts = (u_int64_t)(
						*(p + sizeof(u_int16_t) + t_dsize));
				u_int32_t t_type = (u_int32_t)(
						*(p + sizeof(u_int16_t) + t_dsize + sizeof(u_int64_t)));

				printf("Key: ");
				for (int k = 0; k < key_len; k++) {
					printf("%02x ",
							(u_int8_t)(
									*(key + sizeof(u_int16_t) + sizeof(char) * k)));
				}
				printf("--- Value: ");
				printf("ts: %lld, type: %02d, Destination Key: ", t_ts, t_type);
				for (int k = 0; k < t_dsize; k++) {
					printf("%02x ", (u_int8_t)(*(t_dst + sizeof(char) * k)));
				}
				printf("\n");
			}
		}
	}
	*/
	char db_host[32] = "localhost";
	int db_port = 6379;

	connect_db(db_host, db_port);
	iterate_print();
	close_db();
	return 0;
}
