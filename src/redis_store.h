#ifndef _STORE_H_

#include <inttypes.h>
#include "const.h"
#include "string.h"
#include "hiredis/hiredis.h"

struct node{
	int group;
	unsigned long long uniqueId;
	long long version;
	char name[MAX_STR];
	struct node *next;
};

int connect_db(const char *host, int port);
void close_db();
int insert(char *src, u_int32_t ssize, char *dst, u_int32_t dsize, u_int32_t type, u_int64_t ts, char *val, u_int32_t vsize);
char* get(char *src, u_int32_t ssize, char *dst, u_int32_t dsize, u_int32_t type, u_int64_t ts, u_int32_t *vsize);
int del(char *src, u_int32_t ssize, char *dst, u_int32_t dsize, u_int32_t type, u_int64_t ts);
void iterate_print();
void iterate_json();

#endif
