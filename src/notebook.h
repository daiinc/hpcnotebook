#ifndef _NOTEBOOK_H
#define _NOTEBOOK_H

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stddef.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <string.h>

#include <mpi.h>

#include "utils.h"
#include "redis_store.h"
#include "const.h"

struct file_access {
	char file_name[STR_MAX_LEN];
	int fd, open_flag;
	long long fr_ts, fw_ts, lr_ts, lw_ts;
	struct file_access *next;
};

struct notebook_exec {
	unsigned long long unique_id;
	int pid, parent_pid;
	long long start_ts, end_ts;
	char execname[STR_MAX_LEN], argstr[STR_MAX_LEN], env[STR_MAX_LEN];
	char args[STR_MAX_LEN], retstr[STR_MAX_LEN], execfile[STR_MAX_LEN];
	struct file_access all_opened_files[BUCKET_NUM];
	struct notebook_exec *next;
};

struct notebook_exec_file {
	long long ts;
	int notebook_exec_id;
	char filename[STR_MAX_LEN];
	int access_type; // 0:READ; 1:WRITE; 2:READWRITE; 3:OPEN; 4:CLOSE;
};

struct notebook_version {
	unsigned long long unique_fid;
	unsigned long long unique_pid;
	long long timestamp;

	int version;
	int event;

	struct notebook_version *hash_next;
	struct notebook_version *version_next;
};

typedef struct notebook_message_s {
	int message_header;
	unsigned long long pid, child_pid;
	long long ts1, ts2;
	char execname[STR_MAX_LEN], argstr[STR_MAX_LEN], env[STR_MAX_LEN], retstr[STR_MAX_LEN], execfile[STR_MAX_LEN];
	int fd, flag;
} notebook_message;

#endif
