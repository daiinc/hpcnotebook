#include <stdio.h>
#include <unistd.h>
#include "const.h"

void main(int argc, char **argv) {

	if (argc != 2) {
		printf("Error Input Parameters: ./simulate /path/to/event/files \n");
		return;
	}

	const char *events_file = argv[1]; //this is the event file

	FILE *fd;
	size_t len = 0;
	ssize_t read;

	char *line = NULL;

	fd = fopen(events_file, "r");
	if (fd == NULL)
		return;

	long long start_ts = 0;

	while ((read = getline(&line, &len, fd)) != -1) {
		char *p = (line + 1);

		int child_pid, pid, fd, flag;
		long long timestamp;
		char execname[STR_MAX_LEN], argstr[STR_MAX_LEN], args[STR_MAX_LEN];
		char env[STR_MAX_LEN], filename[STR_MAX_LEN], retstr[STR_MAX_LEN];
		long long fr_ts = 0, lr_ts = 0, fw_ts = 0, lw_ts = 0;

		switch ((*line)) {
		case EVENT_PROCESS_CREAT:
			sscanf(p, "%d %lld %d\n", &child_pid, &timestamp, &pid);
			break;

		case EVENT_PROCESS_EXIT:
			sscanf(p, "%d %lld\n", &pid, &timestamp);
			break;
		case EVENT_EXECVE_START:
			sscanf(p, "%d %lld %s %s %s %s %s\n", &pid, &timestamp,
					execname, argstr, env, filename, args);
			break;

		case EVENT_EXECVE_END:
			sscanf(p, "%d %lld %s %s\n", &pid, &timestamp, retstr, env);
			break;

		case EVENT_OPEN:
			sscanf(p, "%d %lld %s %d %d\n", &pid, &timestamp, filename, &fd,
					&flag);
			break;

		case EVENT_CLOSE_OC:
			sscanf(p, "%d %lld %d\n", &pid, &timestamp, &fd);
			break;

		case EVENT_CLOSE_FL:
			sscanf(p, "%d %lld %d %lld %lld %lld %lld\n", &pid, &timestamp,
					&fd, &fr_ts, &lr_ts, &fw_ts, &lw_ts);
			break;
		}

		if (start_ts == 0){
			printf("%s", line);
			start_ts = timestamp;
		} else {
			long long diff = timestamp - start_ts;
			usleep(1000*diff);
			printf("%s", line);
			start_ts = timestamp;
		}
		//printf("AGGREGATOR GETS LINE: %s END\n", line);
	}
	fclose(fd);

	return;
}
